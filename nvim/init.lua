local plugins_path = vim.fn.stdpath("data") .. "/lazy/"
local function bootstrap(path, project)
    local bootstrap_path = plugins_path .. path
    if not vim.loop.fs_stat(bootstrap_path) then
	vim.fn.system({
	    "git", 
	    "clone",
	    "--filter=blob:none",
	    "--single-branch",
	    "https://github.com/" .. project .. ".git",
	    bootstrap_path,
	})
    end
    vim.opt.rtp:prepend(bootstrap_path)
end

bootstrap("lazy.nvim", "folke/lazy.nvim")
bootstrap("nfnl", "Olical/nfnl")

-- Setup nfnl
require("nfnl").setup()

-- Generate plugins table
local plugins = {
  {
	  { "Olical/nfnl", ft = fennel }
  },
}

-- Add plugins to table
local config_plugins_path = vim.fn.stdpath("config") .. "/fnl/config/plugins"
if vim.loop.fs_stat(config_plugins_path) then
  for file in vim.fs.dir(config_plugins_path) do
    file = file:match("^(.*)%.fnl$")
    plugins[#plugins + 1] = require("config.plugins." .. file)
  end
end

-- Configure Leader Keys
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Configure lazy.nvim
require("lazy").setup(
  plugins,
  { 
	  install = { missing = true },
	  checker = { enabled = true},
  })

-- Require main config
require("config")

-- Commands to run after plugins are loaded (colorscheme, etc) 
--require("post-plugin")
