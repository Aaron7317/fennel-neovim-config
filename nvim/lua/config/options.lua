-- [nfnl] Compiled from fnl/config/options.fnl by https://github.com/Olical/nfnl, do not edit.
vim.opt["number"] = true
vim.opt["relativenumber"] = false
vim.opt["numberwidth"] = 3
vim.opt["list"] = true
vim.opt["listchars"] = {trail = "\194\183", tab = "\226\134\146 ", nbsp = "\194\183"}
vim.opt["shiftwidth"] = indent
vim.opt["termguicolors"] = true
vim.opt["clipboard"] = "unnamedplus"
return {}
