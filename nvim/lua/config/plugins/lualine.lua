-- [nfnl] Compiled from fnl/config/plugins/lualine.fnl by https://github.com/Olical/nfnl, do not edit.
local function config_lualine()
  local lualine = require("lualine")
  return lualine.setup({})
end
return {{"nvim-lualine/lualine.nvim", dependencies = {"nvim-tree/nvim-web-devicons"}, config = config_lualine}}
