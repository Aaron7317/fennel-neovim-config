-- [nfnl] Compiled from fnl/config/plugins/nvim-tree.fnl by https://github.com/Olical/nfnl, do not edit.
local function config_nvim_tree()
  local nvim_tree = require("nvim-tree")
  nvim_tree.setup({})
  return vim.api.nvim_set_keymap("n", "<leader>n", ":NvimTreeToggle<CR>", {silent = true, noremap = true})
end
return {{"nvim-tree/nvim-tree.lua", config = config_nvim_tree}}
