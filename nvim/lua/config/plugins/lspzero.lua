-- [nfnl] Compiled from fnl/config/plugins/lspzero.fnl by https://github.com/Olical/nfnl, do not edit.
local function config_lsp_zero()
  local lsp_zero = require("lsp-zero")
  local function _1_(client, bufnr)
    return lsp_zero.default_keymaps({buffer = bufnr})
  end
  lsp_zero.on_attach(_1_)
  return lsp_zero.setup({})
end
local function config_cmp()
  local cmp = require("cmp")
  return cmp.setup({enabled = true, window_documentation = "native", mapping = {["<CR>"] = cmp.mapping.confirm({select = false}), ["<C-Space>"] = cmp.mapping.complete()}, sources = {{name = "luasnip"}, {name = "nvim_lsp"}, {name = "conjure"}, {name = "lua_ls"}, {name = "fennel_language_server"}}})
end
local function config_mason()
  local mason = require("mason")
  return mason.setup({ui = {icons = {package_installed = "\226\156\147", package_pending = "\226\158\156", package_uninstalled = "\226\156\151"}}})
end
local function config_mason_lsp()
  local mason_lsp = require("mason-lspconfig")
  local lsp_zero = require("lsp-zero")
  lsp_zero.extend_lspconfig({})
  local function _2_(server_name)
    local server = (require("lspconfig"))[server_name]
    return server.setup({})
  end
  local function fennel_language_server()
    local lspconfig = require("lspconfig")
    return lspconfig.fennel_language_server.setup({["default_config.workspace"] = {library = "${3rd}/love2d/library"}})
  end
  return mason_lsp.setup({ensure_installed = {"lua_ls", "fennel_language_server"}, handlers = {_2_, fennel_language_server}})
end
return {{"williamboman/mason.nvim", config = config_mason}, {"williamboman/mason-lspconfig.nvim", dependencies = {"williamboman/mason.nvim"}, config = config_mason_lsp}, {"VonHeikemen/lsp-zero.nvim", config = config_lsp_zero}, {"neovim/nvim-lspconfig"}, {"hrsh7th/nvim-cmp", dependencies = {"hrsh7th/cmp-nvim-lsp", "hrsh7th/cmp-buffer", "L3MON4D3/LuaSnip", "PaterJason/cmp-conjure"}, config = config_cmp}}
