-- [nfnl] Compiled from fnl/config/plugins/init.fnl by https://github.com/Olical/nfnl, do not edit.
local lazy = require("lazy")
local plugins = {{"Olical/nfnl", ft = "fennel"}, {"Mofiqul/dracula.nvim"}}
local lazy_settings = {install = {missing = true}, checker = {enabled = true}}
local function load_plugins()
  return lazy.setup(plugins)
end
return {["load-plugins"] = load_plugins}
