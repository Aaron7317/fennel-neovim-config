-- [nfnl] Compiled from fnl/config/plugins/which-key.fnl by https://github.com/Olical/nfnl, do not edit.
local function _1_()
  vim.opt["timeout"] = true
  vim.opt["timeoutlen"] = 300
  return nil
end
return {{"folke/which-key.nvim", event = "VeryLazy", init = _1_, opts = {}}}
