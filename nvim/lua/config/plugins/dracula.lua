-- [nfnl] Compiled from fnl/config/plugins/dracula.fnl by https://github.com/Olical/nfnl, do not edit.
local function set_colorscheme()
  return vim.cmd("colorscheme dracula")
end
return {{"Mofiqul/dracula.nvim", config = set_colorscheme}}
