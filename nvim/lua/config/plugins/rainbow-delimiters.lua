-- [nfnl] Compiled from fnl/config/plugins/rainbow-delimiters.fnl by https://github.com/Olical/nfnl, do not edit.
local function config_rainbow_delimiters()
  local rainbow_delimiters_setup = require("rainbow-delimiters.setup")
  return rainbow_delimiters_setup.setup({})
end
return {{"HiPhish/rainbow-delimiters.nvim", config = config_rainbow_delimiters}}
