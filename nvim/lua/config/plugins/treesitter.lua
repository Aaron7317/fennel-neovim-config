-- [nfnl] Compiled from fnl/config/plugins/treesitter.fnl by https://github.com/Olical/nfnl, do not edit.
local function treesitter_config()
  local treesitter_configs = require("nvim-treesitter.configs")
  return treesitter_configs.setup({ensure_installed = {"lua", "vim", "c", "bash", "fennel", "json", "make", "nix", "python"}, highlight = {enable = true}, indent = {enable = true}})
end
return {{"nvim-treesitter/nvim-treesitter", build = ":TSUpdate", config = treesitter_config}, {"HiPhish/rainbow-delimiters.nvim"}}
