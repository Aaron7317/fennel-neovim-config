(fn config-lualine []
  (local lualine (require :lualine))
  (lualine.setup {}))

[{1 "nvim-lualine/lualine.nvim"
  :dependencies ["nvim-tree/nvim-web-devicons"]
  :config config-lualine}]
