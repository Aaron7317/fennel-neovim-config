;; Note lua-language-server requires special steps on NixOS
;; https://github.com/williamboman/mason.nvim/issues/1675
;; Install lua-language-server through nixos and link it to mason bin dir
;; cd ~/.local/share/nvim/mason/bin
;; ln -sf $(which lua-language-server) lua-language-server
;; It's really hacky and this needs to be fixed or changed

(fn config-lsp-zero []
  (local lsp-zero (require :lsp-zero))
  (lsp-zero.on_attach 
    (fn [client bufnr]
      (lsp-zero.default_keymaps {:buffer bufnr})))
  (lsp-zero.setup {}))

(fn config-cmp []
  (local cmp (require :cmp))
  (cmp.setup {:enabled true
              :window_documentation "native"
              :mapping {:<CR> (cmp.mapping.confirm {:select false})
                        :<C-Space> (cmp.mapping.complete)}
              :sources [{:name "luasnip"}
                        {:name "nvim_lsp"}
                        {:name "conjure"}
                        {:name "lua_ls"}
                        {:name "fennel_language_server"}]}))

(fn config-mason []
  (local mason (require :mason))
  (mason.setup {:ui {:icons {:package_installed "✓"
                             :package_pending "➜"
                             :package_uninstalled "✗"}}}))

(fn config-mason-lsp []
  (local mason-lsp (require :mason-lspconfig))
  (local lsp-zero (require :lsp-zero))
  (lsp-zero.extend_lspconfig {})
  (mason-lsp.setup {:ensure_installed [:lua_ls :fennel_language_server]
                    :handlers [(fn [server_name]
                                 (local server (. (require :lspconfig) server_name))
                                 (server.setup {}))
			       (fn fennel_language_server []
				 (local lspconfig (require :lspconfig))
				 (lspconfig.fennel_language_server.setup
    				   {:default_config.workspace 
      				        {:library "${3rd}/love2d/library"}}))]}))


[{1 "williamboman/mason.nvim"
  :config config-mason}
 {1 "williamboman/mason-lspconfig.nvim"
  :dependencies ["williamboman/mason.nvim"]
  :config config-mason-lsp}
 {1 "VonHeikemen/lsp-zero.nvim" 
  :config config-lsp-zero }
 {1 "neovim/nvim-lspconfig" }
 {1 "hrsh7th/nvim-cmp"
  :dependencies ["hrsh7th/cmp-nvim-lsp"
		 "hrsh7th/cmp-buffer"
		 "L3MON4D3/LuaSnip"
		 "PaterJason/cmp-conjure"]
  :config config-cmp }]
