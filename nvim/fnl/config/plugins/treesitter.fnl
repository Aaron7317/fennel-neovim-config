(fn treesitter-config []
  (local treesitter-configs (require :nvim-treesitter.configs))
  (treesitter-configs.setup {
    :ensure_installed [:lua :vim :c :bash :fennel :json :make :nix :python]
    :highlight {:enable true}
    :indent {:enable true}}))

[{1 "nvim-treesitter/nvim-treesitter"
  :build ":TSUpdate"
  :config treesitter-config}
 {1 "HiPhish/rainbow-delimiters.nvim"}]
