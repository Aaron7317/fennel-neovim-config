[{1 "folke/which-key.nvim"
  :event "VeryLazy"
  :init 
  (fn []
    (tset vim.opt :timeout true)
    (tset vim.opt :timeoutlen 300))
  :opts {}}]
