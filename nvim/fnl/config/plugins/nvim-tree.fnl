(fn config-nvim-tree []
  (local nvim-tree (require :nvim-tree))
  (nvim-tree.setup {})
  (vim.api.nvim_set_keymap "n" "<leader>n" ":NvimTreeToggle<CR>" {:silent true :noremap true}))

[{1 "nvim-tree/nvim-tree.lua"
  :config config-nvim-tree}]
