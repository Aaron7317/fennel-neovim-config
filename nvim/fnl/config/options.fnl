;; Line numbers
(tset vim.opt :number true)
(tset vim.opt :relativenumber false)
(tset vim.opt :numberwidth 3)

;; Whitespace
(tset vim.opt :list true)
(tset vim.opt :listchars {:trail "·" :tab "→ " :nbsp "·"})

;; Tabs
(tset vim.opt :shiftwidth indent)

;; Highlighting
(tset vim.opt :termguicolors true)

;; Clipboard sharing (requires wl-clipboard package)
(tset vim.opt :clipboard "unnamedplus")

{}
