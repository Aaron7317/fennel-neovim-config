# Fennel Neovim Config

This is a minimal-ish neovim config written in fennel using nfnl. Its a cool idea but practically annoying in my opinion to work with (especially when it comes to lsp and more involved config). I prefer my main configuration to be lua for ease and compatability.
